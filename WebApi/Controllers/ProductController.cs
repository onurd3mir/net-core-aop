﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstarct;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductSevice _productSevice;

        public ProductController(IProductSevice productSevice)
        {
            _productSevice = productSevice;
        }

        

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_productSevice.GetList());
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_productSevice.Get(q => q.ProductID == id));
        }


    }
}