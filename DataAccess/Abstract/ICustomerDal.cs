﻿using Entities.CustomerModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface ICustomerDal:IRepositoryDal<Customer>
    {
        List<CustomerOrder> customerOrders(string customerID);
    }
}
