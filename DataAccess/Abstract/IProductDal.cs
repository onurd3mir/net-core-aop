﻿using Entities.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IProductDal:IRepositoryDal<Product>
    {
        List<BestSellingProducts> BestSellingProducts();
    }
}
