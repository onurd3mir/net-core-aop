﻿using Entities.AdminModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IAdminDal:IRepositoryDal<Admin>
    {
    }
}
