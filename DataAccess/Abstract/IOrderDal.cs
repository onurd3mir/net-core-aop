﻿using Entities.OrderModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface IOrderDal:IRepositoryDal<Order>
    {
        List<OrderDetailInformation> orderDetail(int orderID = 0);
    }
}
