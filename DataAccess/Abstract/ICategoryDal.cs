﻿using Entities.CategoryModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Abstract
{
    public interface ICategoryDal:IRepositoryDal<Category>
    {
    }
}
