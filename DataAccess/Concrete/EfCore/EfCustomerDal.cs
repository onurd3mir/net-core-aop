﻿using DataAccess.Abstract;
using Entities.CustomerModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DataAccess.Concrete.EfCore
{
    public class EfCustomerDal : EfCoreGenericRepository<Customer, IronContext>, ICustomerDal
    {
        public List<CustomerOrder> customerOrders(string customerID)
        {
            using (var context = new IronContext())
            {
                var result = context.customerOrders.FromSql($"CustOrders {customerID} ").ToList();
                return result;
            }
        }
    }
}
