﻿using DataAccess.Abstract;
using Entities.AdminModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Concrete.EfCore
{
    public class EfAdminDal:EfCoreGenericRepository<Admin,IronContext>,IAdminDal
    {
    }
}
