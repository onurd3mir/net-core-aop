﻿using DataAccess.Abstract;
using Entities.ProductModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataAccess.Concrete.EfCore
{
    public class EfProductDal : EfCoreGenericRepository<Product, IronContext>, IProductDal
    {
        public List<BestSellingProducts> BestSellingProducts()
        {
            using (var context = new IronContext())
            {
                return context.BestSellingProducts.FromSql("sp_SLC_BestSellignProducts").ToList();
            } 
        }
    }
}
