﻿using DataAccess.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataAccess.Concrete.EfCore
{
    public class EfCoreGenericRepository<T, TContext> : IRepositoryDal<T>
        where T : class
        where TContext : DbContext, new()
    {
        public int Create(T Entity)
        {
            using (var context = new TContext())
            {
                context.Set<T>().Add(Entity);
                return context.SaveChanges();
            }
        }

        public int Delete(T Entity)
        {
            using (var context = new TContext())
            {
                context.Set<T>().Remove(Entity);
                return context.SaveChanges();
            }
        }

        public T Get(Expression<Func<T, bool>> filter)
        {
            using (var context = new TContext())
            {
                return context.Set<T>().AsNoTracking().Where(filter).SingleOrDefault();
            }
        }

        public List<T> GetList(Expression<Func<T, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                //return context.Set<T>().AsNoTracking().ToList();

                return filter == null
                         ? context.Set<T>().AsNoTracking().ToList()
                         : context.Set<T>().AsNoTracking().Where(filter).ToList();
            }
        }

        public IQueryable<T> GetQuery(string sql)
        {
            using (var context = new TContext())
            {
                return context.Set<T>().FromSql(sql).AsNoTracking().AsQueryable();
            } 
        }

        public int Update(T Entity)
        {
            using (var context = new TContext())
            {
                context.Entry(Entity).State = EntityState.Modified;
                return context.SaveChanges();
            }
        }
    }
}
