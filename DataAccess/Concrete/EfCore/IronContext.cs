﻿using Entities.AdminModels;
using Entities.CategoryModels;
using Entities.CustomerModels;
using Entities.OrderModels;
using Entities.ProductModels;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EfCore
{
    public class IronContext : DbContext
    {

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Admin> Admins { get; set; }

        #region customModel

        public DbQuery<BestSellingProducts> BestSellingProducts { get; set; }
        public DbQuery<OrderDetailInformation> orderDetail { get; set; }
        public DbQuery<CustomerOrder> customerOrders { get; set; }

        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-P3TICEA;Database=Northwind;integrated security=true;");
        }
    }
}
