﻿using DataAccess.Abstract;
using Entities.OrderModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DataAccess.Concrete.EfCore
{
    public class EfOrderDal : EfCoreGenericRepository<Order, IronContext>, IOrderDal
    {
        public List<OrderDetailInformation> orderDetail(int orderID = 0)
        {
            using (var context = new IronContext())
            { 
                return context.orderDetail.FromSql($"OrderDetail {orderID}").ToList();
            }
        }
    }
}
