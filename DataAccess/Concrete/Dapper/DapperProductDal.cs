﻿using Dapper;
using DataAccess.Abstract;
using Entities.ProductModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataAccess.Concrete.Dapper
{
    public class DapperProductDal : IProductDal
    {
        private readonly string conn = @"Server=DESKTOP-P3TICEA;Database=Northwind;integrated security=true;";

        public List<BestSellingProducts> BestSellingProducts()
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Query<BestSellingProducts>("sp_SLC_BestSellignProducts").ToList();
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public int Create(Product Entity)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Execute($"delete from Products where ProductID={Entity.ProductID}");
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public int Delete(Product Entity)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Execute($"delete from Products where ProductID={Entity.ProductID}");
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public Product Get(Expression<Func<Product, bool>> filter)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Query<Product>("select * from Products (nolock)").AsQueryable().FirstOrDefault(filter);
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public List<Product> GetList(Expression<Func<Product, bool>> filter = null)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();

                    return filter == null
                        ? context.Query<Product>("select * from Products (nolock)").ToList()
                        : context.Query<Product>("select * from Products (nolock)").AsQueryable().Where(filter).ToList();
 
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public IQueryable<Product> GetQuery(string sql)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Query<Product>(sql).AsQueryable();
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }   
        }

        public int Update(Product Entity)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Execute($"delete from Products where ProductID={Entity.ProductID}");
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }
    }
}
