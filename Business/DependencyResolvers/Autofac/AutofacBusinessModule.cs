﻿using Autofac;
using Autofac.Extras.AggregateService;
using Autofac.Extras.DynamicProxy;
using Business.Abstarct;
using Business.Concrete;
using Core.Aspects.Autofac.Cache;
using Core.Aspects.Autofac.DebugLog;
using Core.Aspects.Autofac.Performance;
using Core.CrossCuttingConcerns.Caching;
using Core.CrossCuttingConcerns.Caching.Microsoft;
using DataAccess.Abstract;
using DataAccess.Concrete.Dapper;
using DataAccess.Concrete.EfCore;

namespace Business.DependencyResolvers.Autofac
{
    public class AutofacBusinessModule : Module
    { 
        protected override void Load(ContainerBuilder builder)
        {            
            //constructor hell çözüm
            builder.RegisterAggregateService<IMyAggregateService>();

            #region Interceptor

            builder.RegisterType<MemoryCacheManager>()
            .As<ICacheManager>()
            .SingleInstance();

            builder.RegisterType<CacheInterceptorAspect>()
            .SingleInstance();

            builder.RegisterType<DebugInterceptorAspect>()
            .SingleInstance();

            builder.RegisterType<PerformanceInterceptionAspect>()
            .SingleInstance();

            #endregion

            #region DataAccess

            builder.RegisterType<EfProductDal>().As<IProductDal>();
            //builder.RegisterType<DapperProductDal>().As<IProductDal>();
            builder.RegisterType<EfCategoryDal>().As<ICategoryDal>();
            builder.RegisterType<EfOrderDal>().As<IOrderDal>();
            builder.RegisterType<EfCustomerDal>().As<ICustomerDal>();
            builder.RegisterType<EfAdminDal>().As<IAdminDal>();

            #endregion

            #region Business 

            builder.RegisterType<ProductManager>().As<IProductSevice>()
            .EnableInterfaceInterceptors()
            //.InterceptedBy(typeof(PerformanceInterceptionAspect))
            //.InterceptedBy(typeof(DebugInterceptorAspect))
            .InterceptedBy(typeof(CacheInterceptorAspect));
            
            builder.RegisterType<CategoryManager>().As<ICategoryService>();
            builder.RegisterType<DapperManager>().As<IDapperService>();
            builder.RegisterType<OrderManger>().As<IOrderService>();
            builder.RegisterType<CustomerManager>().As<ICustomerService>();
            builder.RegisterType<AdminManager>().As<IAdminService>();

            #endregion

        }
    }
}
