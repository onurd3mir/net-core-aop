﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Abstarct
{
    public interface IRepositorySevice<T>
    {
        T Get(Expression<Func<T, bool>> filter);
        List<T> GetList(Expression<Func<T, bool>> filter = null);
        IQueryable<T> GetQuery(string sql);

        int Create(T Entity);
        int Update(T Entity);
        int Delete(T Entity);
    }
}
