﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstarct
{
    public interface IMyAggregateService
    {
        //constructor hell çözüm
        IDapperService dapperService { get; }
        IProductSevice productSevice { get; }
        ICategoryService categoryService { get; }
        IOrderService orderService { get; }
        ICustomerService customerService { get; }
        IAdminService adminService { get; }
    }
}
