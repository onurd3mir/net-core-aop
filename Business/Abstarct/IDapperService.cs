﻿using System.Linq;

namespace Business.Abstarct
{
    public interface IDapperService
    {
        IQueryable<T> GetQuery<T>(string sql);

        int ExecuteQuery(string sql);
    }
}
