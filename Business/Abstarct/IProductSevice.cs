﻿using Core.Aspects.Autofac.Cache;
using Core.Aspects.Autofac.DebugLog;
using Entities.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Business.Abstarct
{
    public interface IProductSevice
    {

        Product Get(Expression<Func<Product, bool>> filter);
        List<Product> GetList(Expression<Func<Product, bool>> filter = null);
        IQueryable<Product> GetQuery(string sql);

        int Create(Product Entity);
        int Update(Product Entity);

        [Debug]
        int Delete(Product Entity);

        [Cache(Duration = 20)]
        List<BestSellingProducts> BestSellingProducts();

    }
}
