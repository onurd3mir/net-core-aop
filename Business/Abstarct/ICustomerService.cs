﻿using Entities.CustomerModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstarct
{
    public interface ICustomerService:IRepositorySevice<Customer>
    {
        List<CustomerOrder> customerOrders(string customerID);
    }
}
