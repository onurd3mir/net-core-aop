﻿using Entities.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Abstarct
{
    public interface IOrderService
    {
        Order Get(Expression<Func<Order, bool>> filter);
        List<Order> GetList(Expression<Func<Order, bool>> filter = null);
        IQueryable<Order> GetQuery(string sql);

        List<OrderDetailInformation> orderDetail(int orderID = 0);

        int Create(Order Entity);
        int Update(Order Entity); 
        int Delete(Order Entity); 
    }
}
