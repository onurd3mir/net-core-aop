﻿using Business.Abstarct;
using DataAccess.Abstract;
using Entities.ProductModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Business.Concrete
{
    public class ProductManager : IProductSevice
    {
        private IProductDal _productDal;

        public ProductManager(IProductDal productDal)
        {
            _productDal = productDal;
        }

        public List<BestSellingProducts> BestSellingProducts()
        {
            return _productDal.BestSellingProducts();
        }

        public int Create(Product Entity)
        {
            return _productDal.Create(Entity);
        }

        public int Delete(Product Entity)
        {
            return _productDal.Delete(Entity);
        }

        public Product Get(Expression<Func<Product, bool>> filter)
        {
            return _productDal.Get(filter);
        }

        public List<Product> GetList(Expression<Func<Product, bool>> filter = null)
        {
            return _productDal.GetList(filter);
        }

        public IQueryable<Product> GetQuery(string sql)
        {
            return _productDal.GetQuery(sql);
        }

        public int Update(Product Entity)
        {
            return _productDal.Update(Entity);
        }
    }
}
