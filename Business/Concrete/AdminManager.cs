﻿using Business.Abstarct;
using DataAccess.Abstract;
using Entities.AdminModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Concrete
{
    public class AdminManager : IAdminService
    {
        private IAdminDal _adminDal;

        public AdminManager(IAdminDal adminDal)
        {
            _adminDal = adminDal;
        }

        public int Create(Admin Entity)
        {
            return _adminDal.Create(Entity);
        }

        public int Delete(Admin Entity)
        {
            return _adminDal.Delete(Entity);
        }

        public Admin Get(Expression<Func<Admin, bool>> filter)
        {
            return _adminDal.Get(filter);
        }

        public List<Admin> GetList(Expression<Func<Admin, bool>> filter = null)
        {
            return _adminDal.GetList(filter);
        }

        public IQueryable<Admin> GetQuery(string sql)
        {
            throw new NotImplementedException();
        }

        public int Update(Admin Entity)
        {
            return _adminDal.Update(Entity);
        }
    }
}
