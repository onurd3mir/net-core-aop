﻿using Business.Abstarct;
using DataAccess.Abstract;
using Entities.CategoryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private ICategoryDal _categoryDal;

        public CategoryManager(ICategoryDal categoryDal)
        {
            _categoryDal = categoryDal;
        }

        public int Create(Category Entity)
        {
            return _categoryDal.Create(Entity);
        }

        public int Delete(Category Entity)
        {
            return _categoryDal.Delete(Entity);
        }

        public Category Get(Expression<Func<Category, bool>> filter)
        {
            return _categoryDal.Get(filter);
        }

        public List<Category> GetList(Expression<Func<Category, bool>> filter = null)
        {
            return _categoryDal.GetList(filter);
        }

        public IQueryable<Category> GetQuery(string sql)
        {
            return _categoryDal.GetQuery(sql);
        }

        public int Update(Category Entity)
        {
            return _categoryDal.Update(Entity);
        }
    }
}
