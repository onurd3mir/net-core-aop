﻿using Business.Abstarct;
using Dapper;
using System;
using System.Data.SqlClient;
using System.Linq;

namespace Business.Concrete
{
    public class DapperManager : IDapperService
    {
        private readonly string conn = @"Server=DESKTOP-P3TICEA;Database=Northwind;integrated security=true;";

        public int ExecuteQuery(string sql)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Execute(sql);
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }

        public IQueryable<T> GetQuery<T>(string sql)
        {
            using (var context = new SqlConnection(conn))
            {
                try
                {
                    context.Open();
                    return context.Query<T>(sql).AsQueryable();
                }
                catch (Exception err)
                {
                    throw new NotImplementedException(err.Message);
                }
                finally { context.Close(); }
            }
        }
    }
}
