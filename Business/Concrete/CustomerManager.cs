﻿using Business.Abstarct;
using DataAccess.Abstract;
using Entities.CustomerModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Concrete
{
    public class CustomerManager : ICustomerService
    {
        private ICustomerDal _customerDal;

        public CustomerManager(ICustomerDal customerDal)
        {
            _customerDal = customerDal;
        }

        public int Create(Customer Entity)
        {
            return _customerDal.Create(Entity);
        }

        public List<CustomerOrder> customerOrders(string customerID)
        {
            return _customerDal.customerOrders(customerID);
        }

        public int Delete(Customer Entity)
        {
            return _customerDal.Delete(Entity);
        }

        public Customer Get(Expression<Func<Customer, bool>> filter)
        {
            return _customerDal.Get(filter);
        }

        public List<Customer> GetList(Expression<Func<Customer, bool>> filter = null)
        {
            return _customerDal.GetList(filter);
        }

        public IQueryable<Customer> GetQuery(string sql)
        {
            throw new NotImplementedException();
        }

        public int Update(Customer Entity)
        {
            return _customerDal.Update(Entity);
        }
    }
}
