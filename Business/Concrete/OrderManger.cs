﻿using Business.Abstarct;
using DataAccess.Abstract;
using Entities.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Business.Concrete
{
    public class OrderManger : IOrderService
    {
        private IOrderDal _orderDal;

        public OrderManger(IOrderDal orderDal)
        {
            _orderDal = orderDal;
        }

        public int Create(Order Entity)
        {
            return _orderDal.Create(Entity);
        }

        public int Delete(Order Entity)
        {
            return _orderDal.Delete(Entity);
        }

        public Order Get(Expression<Func<Order, bool>> filter)
        {
            return _orderDal.Get(filter);
        }

        public List<Order> GetList(Expression<Func<Order, bool>> filter = null)
        {
            return _orderDal.GetList(filter);
        }

        public IQueryable<Order> GetQuery(string sql)
        {
            throw new NotImplementedException();
        }

        public List<OrderDetailInformation> orderDetail(int orderID = 0)
        {
            return _orderDal.orderDetail(orderID);
        }

        public int Update(Order Entity)
        {
            return _orderDal.Update(Entity);
        }
    }
}
