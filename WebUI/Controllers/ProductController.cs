﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstarct;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    public class ProductController : BaseController
    {
        private IMyAggregateService _myAggregateService;

        public ProductController(IMyAggregateService myAggregateService)
        {
            _myAggregateService = myAggregateService;
        }

        public IActionResult Index()
        {
            //var result = _myAggregateService.productSevice.GetList(q=>q.CategoryID==3 && q.UnitsInStock>=15);
            var result = _myAggregateService.productSevice.GetList();
            return View(result);
        }

        public IActionResult Detail(int id)
        {
            var result = _myAggregateService.productSevice.Get(q => q.ProductID == id);
            return View(result);
        }

        public IActionResult BestSellingProducts()
        {
            var result = _myAggregateService.productSevice.BestSellingProducts();
            return Json(result);
        }

        public IActionResult Delete(int id)
        {
            var result = _myAggregateService.productSevice.Get(q => q.ProductID == id);

            if (result!=null)
            {
                _myAggregateService.productSevice.Delete(result);
                return RedirectToAction("index");
            }
            else
            {
                return View();
            }
        }

    }
}