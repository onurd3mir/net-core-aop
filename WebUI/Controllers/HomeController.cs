﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Business.Abstarct;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class HomeController : BaseController
    {
        private IMyAggregateService _myAggregateService;

        public HomeController(IMyAggregateService myAggregateService)
        {
            _myAggregateService = myAggregateService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(AdminLoginModel adminLoginModel)
        {
            if(!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                var result = _myAggregateService.adminService.Get(q => q.UserName == adminLoginModel.UserName && q.Password == adminLoginModel.Password);

                if(result==null)
                {
                    ModelState.AddModelError("", "Kullanıcı Giriş Başarısız");
                    return View();
                }
                else
                {
                    var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                    identity.AddClaim(new Claim(ClaimTypes.Name, result.UserName));
                    identity.AddClaim(new Claim("UserId", result.Admin_ID.ToString()));
                    //identity.AddClaim(new Claim(ClaimTypes.GivenName, user.FirstName));
                    //identity.AddClaim(new Claim(ClaimTypes.Surname, user.LastName));

                    //foreach (var role in user.Roles)
                    //{
                    //    identity.AddClaim(new Claim(ClaimTypes.Role, role.Role));
                    //}

                    identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                    //identity.AddClaim(new Claim(ClaimTypes.Role, "user"));

                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties
                    {
                        IsPersistent = false,
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(40),

                    });

                    return RedirectToAction("orders", "admin");

                }
            }
;
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
