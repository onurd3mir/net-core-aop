﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstarct;
using Entities.CategoryModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [Authorize]
    public class AdminController : BaseController
    {       
        private IMyAggregateService _myAggregateService;

        public AdminController(IMyAggregateService myAggregateService)
        {
            _myAggregateService = myAggregateService;
        }

        public IActionResult Categories()
        { 
            var result = _myAggregateService.dapperService.GetQuery<Category>("select c.CategoryID,c.CategoryName from Categories c (nolock)").ToList();
            return View(result);
        }

        public IActionResult Orders()
        {
            var result = _myAggregateService.orderService.GetList().Take(20);
            return View(result);
        }

        public IActionResult OrderDetail(int id)
        {
            var result = _myAggregateService.orderService.orderDetail(id);
            return View(result);
        }

        public IActionResult Customer()
        {
            var result = _myAggregateService.customerService.GetList();
            return View(result);
        }

        public IActionResult CustomerOrders(string id)
        {
            var result = _myAggregateService.customerService.customerOrders(id);
            return View(result);
        }

        [Authorize(Roles = "admin")]
        public IActionResult Order(int id)
        {
            var result = _myAggregateService.orderService.Get(q => q.OrderID == id);
            return View(result);
        }

        public IActionResult Admins()
        {
            var result = _myAggregateService.adminService.GetList();
            return View(result);
        }

    }
}