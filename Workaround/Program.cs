﻿using Autofac;
using Autofac.Extras.DynamicProxy;
using Business.Abstarct;
using Business.Concrete;
using Castle.DynamicProxy;
using DataAccess.Abstract;
using DataAccess.Concrete.Dapper;
using DataAccess.Concrete.EfCore;
using System;

namespace Workaround
{
    class Program
    {
        static void Main(string[] args)
        {


            var builder = new ContainerBuilder();

            builder.RegisterType<EfProductDal>()
                               .As<IProductDal>();

            builder.RegisterType<ProductManager>().As<IProductSevice>()
                .EnableInterfaceInterceptors();

            //builder.Register(c => new CallLogger(Console.Out))
            //               .Named<IInterceptor>("log-calls");


            //builder.Register(c => new MethodInterceptionAspect());

            var container = builder.Build();



            var productManager = container.Resolve<IProductSevice>();


            foreach (var item in productManager.GetList(q => q.UnitsInStock > 15 && q.UnitsInStock < 30))
            {
                Console.WriteLine(item.ProductID + "-" + item.ProductName);
            }


            //ProductManager productManager = new ProductManager(new EfProductDal());
            //ProductManager productManager = new ProductManager(new DapperProductDal());

            //foreach (var item in productManager.GetList(q => q.UnitsInStock > 15 && q.UnitsInStock < 30))
            //{
            //    Console.WriteLine(item.ProductID + "-" + item.ProductName);
            //}

            //var row = productManager.Get(q => q.ProductID == 70);

            //Console.WriteLine(row.ProductName);

            Console.ReadKey();
        }
    }
}
