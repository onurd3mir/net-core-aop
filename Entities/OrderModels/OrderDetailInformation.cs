﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.OrderModels
{
    public class OrderDetailInformation
    {
        public int OrderID { get; set; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }
        public float Discount { get; set; }
        public decimal TotalAmount { get; set; }
    }
}
