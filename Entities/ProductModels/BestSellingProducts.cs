﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ProductModels
{
   
    public class BestSellingProducts
    {
        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public decimal UnitPrice { get; set; }

        public short UnitsInStock { get; set; }

        public int SalesQuantity { get; set; }

        public string CategoryName { get; set; }
    }
}
