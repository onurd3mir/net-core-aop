﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ProductModels
{
    public class Product
    {
        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public int SupplierID { get; set; }

        public int CategoryID { get; set; }

        public decimal UnitPrice { get; set; }

        public short UnitsInStock { get; set; }
        
    }
}
