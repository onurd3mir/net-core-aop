﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.CustomerModels
{
    public class CustomerOrder
    {
        public int OrderID { get; set; }
        
        public DateTime OrderDate { get; set; }

        public DateTime RequiredDate { get; set; }

        public DateTime? ShippedDate { get; set; }
    }
}
