﻿using Castle.DynamicProxy;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Reflection;

namespace Core.Aspects.Autofac.DebugLog
{
    public class DebugInterceptorAspect : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var debugAttribute = invocation.Method.GetCustomAttribute<DebugAttribute>();

            if (debugAttribute == null)
            {
                invocation.Proceed();
                return;
            }
            else
            {
                var parametersJson = JsonConvert.SerializeObject(invocation.Arguments);
                Debug.WriteLine("Request of " + invocation.Method.Name + " is " + parametersJson);
                invocation.Proceed();
            }   
        }
    }
}
