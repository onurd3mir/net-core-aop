﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Aspects.Autofac.DebugLog
{
    [AttributeUsage(AttributeTargets.Method)]
    public class DebugAttribute:Attribute
    {
    }
}
