﻿using Castle.DynamicProxy;
using System;
using System.Diagnostics;

namespace Core.Aspects.Autofac.Performance
{
    public class PerformanceInterceptionAspect : IInterceptor
    {
        private readonly Stopwatch _stopwatch;

        public PerformanceInterceptionAspect()
        {
            _stopwatch = Activator.CreateInstance<Stopwatch>();
        }
        
        public void Intercept(IInvocation invocation)
        {
            _stopwatch.Start();

            invocation.Proceed();

            string logLine = string.Format($"Performance : {invocation.Method.DeclaringType.FullName}.{invocation.Method.Name} --> {_stopwatch.Elapsed.TotalSeconds}");
           
            _stopwatch.Stop();

            Debug.WriteLine(logLine);

            _stopwatch.Reset();

        }
        


    }
}
