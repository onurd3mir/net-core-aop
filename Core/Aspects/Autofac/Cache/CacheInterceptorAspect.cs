﻿using Castle.DynamicProxy;
using Core.CrossCuttingConcerns.Caching;
using System.Linq;
using System.Reflection;

namespace Core.Aspects.Autofac.Cache
{
    public class CacheInterceptorAspect : IInterceptor
    {
        private readonly ICacheManager _cacheManager;

        public CacheInterceptorAspect(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public void Intercept(IInvocation invocation)
        {
            var cacheAttribute = invocation.Method.GetCustomAttribute<CacheAttribute>();

            if(cacheAttribute==null)
            {
                invocation.Proceed();
                return;
            }
            else
            {
                var methodName = string.Format($"{invocation.Method.ReflectedType.FullName}.{invocation.Method.Name}");

                var arguments = invocation.Arguments.ToList();

                var key = $"{methodName}({string.Join(",", arguments.Select(x => x?.ToString() ?? "<Null>"))})";

                if (_cacheManager.Get(key)!=null)
                {
                    invocation.ReturnValue = _cacheManager.Get(key);
                    return;
                }

                invocation.Proceed();
                _cacheManager.Put(key, invocation.ReturnValue,cacheAttribute.Duration);
            }

        }
    }
}
