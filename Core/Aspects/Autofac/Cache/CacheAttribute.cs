﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Aspects.Autofac.Cache
{
    [AttributeUsage(AttributeTargets.Method)]
    public class CacheAttribute: Attribute
    {
        public int Duration { get; set; }
        
    }
}
