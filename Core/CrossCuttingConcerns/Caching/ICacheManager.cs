﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.CrossCuttingConcerns.Caching
{
    public interface ICacheManager
    {
        object Get(string key);

        void Put(string key, object value, int duration);

        bool Contains(string key);
    }
}
